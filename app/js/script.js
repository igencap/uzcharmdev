$(document).ready(function () {


    /**********************
     **********************
        BURGER
    **********************
    **********************/
    $('.burger__btn').click(function () {
        $('.menu__wrapper').toggleClass('active');
    })

    $('#menu-close').click(function () {
        $('.menu__wrapper').removeClass('active');
    })

    /**********************
    **********************
        GALLERY
    **********************
    **********************/
    const tabs2 = document.querySelectorAll('.gallery__tab-link');
    const all_list2 = document.querySelectorAll('.gallery__list');

    tabs2.forEach((tab2, index2) => {
        tab2.addEventListener('click', function () {
            tabs2.forEach(tab2 => { tab2.classList.remove('active') })
            tab2.classList.add('active')

            all_list2.forEach(list => { list.classList.remove('active') })
            all_list2[index2].classList.add('active')
        })
    })


    $('.gallery__list').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        }
    });


    /**********************
    **********************
        FEEDBACK
    **********************
    **********************/
    $('.feedback__item-img').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });


    /**********************
     **********************
        REGISTRATION PARTICIPANT
    **********************
    **********************/
    let showPriceBtn = document.getElementById('partakers__btn');
    let priceTag = document.getElementById('price-tag__container');
    let detailsTable = document.getElementById('details-table');
    let registrationContainer = document.getElementById('partakers__registration-container');

    if (showPriceBtn) {
        showPriceBtn.addEventListener('click', function () {
            priceTag.classList.add('active');
            detailsTable.classList.add('active');
            if (registrationContainer.className.match("col-lg-12 col-12")) {
                registrationContainer.className = "col-lg-9 col-12"
            }

            showPriceBtn.innerHTML = 'Изменить выставочный стенд'
        })

    }
});

/**********************
**********************
    ACCARDION
**********************
**********************/

const accordionTitles = document.querySelectorAll(".accardion-item");

if (accordionTitles) {
    accordionTitles.forEach((accordionTitle) => {
        accordionTitle.addEventListener("click", () => {
            if (accordionTitle.classList.contains("accardion-active")) {
                accordionTitle.classList.remove("accardion-active");
            } else {
                const accordionTitlesWithIsOpen = document.querySelectorAll(".accardion-active");
                accordionTitlesWithIsOpen.forEach((accordionTitleWithIsOpen) => {
                    accordionTitleWithIsOpen.classList.remove("accardion-active");
                });
                accordionTitle.classList.add("accardion-active");
            }
        });
    });
}

/**********************
  **********************
      SCHEDULE
  **********************
  **********************/
const tabs = document.querySelectorAll('.schedule__tab-link');
const all_list = document.querySelectorAll('.schedule__list');

if (tabs) {
    tabs.forEach((tab, index) => {
        tab.addEventListener('click', function () {
            tabs.forEach(tab => { tab.classList.remove('active') })
            tab.classList.add('active')

            all_list.forEach(list => { list.classList.remove('active') })
            all_list[index].classList.add('active')
        })
    })
}

if (tabs) {
    tabs[tabs.length - 1].addEventListener('click', function () {
        all_list.forEach(list => { list.classList.add('active') });
    });
}